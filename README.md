Dotclear 2 theme: SCENE
=======================

![screenshot](https://bytebucket.org/sylvain_www/scene/raw/e72738b8c64f19d336c9ade95457cb60f528ebe3/screenshot.jpg)

A theme for [dotclear 2][link_dotclear]. Tweaked to bring CMS power to a resume-style web site. Ideal for the personnal site of an artist. This project is part of the revamp of the web signature of a friend of mine, baryton at the opera.

[Visit Marc's web site][link_marc].

[link_dotclear]: http://dotclear.org
[link_marc]: http://marc-souchet.fr

