<?php
# -- BEGIN LICENSE BLOCK ---------------------------------------
# This file is part of Scene, a theme for Dotclear
#
# Copyright (c) 2011 - Association Dotclear
# Licensed under the GPL version 2.0 license.
# See LICENSE file or
# http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
#
# -- END LICENSE BLOCK -----------------------------------------
if (!defined('DC_RC_PATH')) { return; }

$this->registerModule(
    "Scene",
    "A dotclear theme designed for Marc Souchet",
    "Sylvain Killian",
    "2.0",
    array(
        'type' => 'theme'
    )
);
