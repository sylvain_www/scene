<?php
# -- BEGIN LICENSE BLOCK ---------------------------------------
# This file is part of Scene, a theme for Dotclear
#
# Copyright (c) 2011 - Association Dotclear
# Licensed under the GPL version 2.0 license.
# See LICENSE file or
# http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
#
# -- END LICENSE BLOCK -----------------------------------------

if (!defined('DC_RC_PATH')) { return; }

# Templates
$core->tpl->addValue('EntryStringContent',array('tplSceneTheme','EntryStringContent'));
$core->tpl->addValue('publicURL',array('tplSceneTheme','publicURL'));

include_once("themes/scene/js/icalparser/src/EventsList.php");
include_once("themes/scene/js/icalparser/src/Freq.php");
include_once("themes/scene/js/icalparser/src/Recurrence.php");
include_once("themes/scene/js/icalparser/src/IcalParser.php");


class tplSceneTheme
{
    public static function EntryGetValue($table, $key, $defaultval, &$val)
    {
        if (array_key_exists($key, $table)) {
            $val = $table[$key];
            return TRUE;
        } else {
            $val = $defaultval;
            return FALSE;
        }
    }

    public static function EntryStringContent($attr)
    {
        $f = $GLOBALS['core']->tpl->getFilters($attr);
        return sprintf($f,'$_ctx->posts->getContent(0)');
    }

    public static function publicURL($attr)
    {
        $f = $GLOBALS['core']->tpl->getFilters($attr);
        return sprintf($f,'$core->blog->settings->system->public_url');
    }

    // helper to get calendar data from icloud
    //See Updates and explanation at: https://github.com/tazotodua/useful-php-scripts/
    public static function get_remote_data($url, $post_paramtrs=false)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        if($post_paramtrs)
        {
            curl_setopt($c, CURLOPT_POST,TRUE);
            curl_setopt($c, CURLOPT_POSTFIELDS, "var1=bla&".$post_paramtrs );
        }
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST,false);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($c, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; rv:33.0) Gecko/20100101 Firefox/33.0");
        curl_setopt($c, CURLOPT_COOKIE, 'CookieName1=Value;');
        curl_setopt($c, CURLOPT_MAXREDIRS, 10);
        $follow_allowed= ( ini_get('open_basedir') || ini_get('safe_mode')) ? false:true;
        if ($follow_allowed)
        {
            curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
        }
        curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 9);
        curl_setopt($c, CURLOPT_REFERER, $url);
        curl_setopt($c, CURLOPT_TIMEOUT, 60);
        curl_setopt($c, CURLOPT_AUTOREFERER, true);
        curl_setopt($c, CURLOPT_ENCODING, 'gzip,deflate');
        $data=curl_exec($c);
        $status=curl_getinfo($c);
        curl_close($c);
        preg_match('/(http(|s)):\/\/(.*?)\/(.*\/|)/si',  $status['url'],$link); $data=preg_replace('/(src|href|action)=(\'|\")((?!(http|https|javascript:|\/\/|\/)).*?)(\'|\")/si','$1=$2'.$link[0].'$3$4$5', $data);   $data=preg_replace('/(src|href|action)=(\'|\")((?!(http|https|javascript:|\/\/)).*?)(\'|\")/si','$1=$2'.$link[1].'://'.$link[3].'$3$4$5', $data);
        if($status['http_code']==200)
        {
            return $data;
        }
        elseif($status['http_code']==301 || $status['http_code']==302)
        {
            if (!$follow_allowed)
            {
                if (!empty($status['redirect_url']))
                {
                    $redirURL=$status['redirect_url'];
                }
                else
                {
                    preg_match('/href\=\"(.*?)\"/si',$data,$m);
                    if (!empty($m[1]))
                    {
                        $redirURL=$m[1];
                    }
                }
                if(!empty($redirURL))
                {
                    return call_user_func( __FUNCTION__, $redirURL, $post_paramtrs);
                }
            }
        }
        return "";
    }

    public static function create_calendar_fragment() {
        $fmt_avenir = new IntlDateFormatter(
            'fr_FR',
            IntlDateFormatter::FULL,
            IntlDateFormatter::FULL,
            'Europe/Paris',
            IntlDateFormatter::GREGORIAN,
            'EEEE dd MMMM YYYY à HH:mm'
        );
        $fmt_prochain = new IntlDateFormatter(
            'fr_FR',
            IntlDateFormatter::FULL,
            IntlDateFormatter::FULL,
            'Europe/Paris',
            IntlDateFormatter::GREGORIAN,
            'd MMMM'
        );

        $data_url = 'https://p51-caldav.icloud.com/published/2/MTk5OTUwNzQ2MTk5OTUwN1S_v6UvoLVEo7s0RjFJ8V7Yp-0jxdwm5jPekHeklRNE6vf3255BVq48_vckha55xD7KEDoaTB4tpiHoqQ3QGQs';
        $ics_data = tplSceneTheme::get_remote_data($data_url);
        if (!$ics_data) {
            // error: leave previous data intact
            return;
        }
        $ICS = 'public/calendar.ics';
        file_put_contents($ICS, $ics_data);

        $cal = new IcalParser();
        $results = $cal->parseFile($ICS);

        function format_($x, $style) {
            // workaround utf-8 style new line not replaced correctly
            $y = str_replace("\xE2\x80\xA8", "\n", $x);
            return '<div class="'.$style.'">'.nl2br(htmlentities($y, ENT_COMPAT | ENT_HTML401, 'UTF-8')).'</div>';
        }

        $avenir = "<table class='avenir_table'><tbody>";
        $prochain = "";
        $first = true;

        foreach ($cal->getEvents()->sorted() as $ev) {
            // skip outdated events
            if ($ev['DTSTART']->getTimestamp() < time()) {
                continue;
            }

            if ($first) {
                $prochain .= format_($ev['SUMMARY'], 'prochainement_summary');

                $attachement = $ev['ATTACH'];
                if ($attachement) {
                    $prochain .= '<div class="image_section">';
                    $prochain .= '<a href="';
                    $prochain .= $attachement;
                    $prochain .= '" title="" class="prochainement_image">';
                    $prochain .= '<div class="prochainement_image">';
                    $prochain .= '<span class="helper"></span><img src="';
                    $prochain .= $attachement;
                    $prochain .= '"></div></a></div>';
                }

                $prochain .= format_(
                    "le ".$fmt_prochain->format($ev['DTSTART']),
                    'prochainement_date'
                );

                $link = $ev['URL'];
                if ($link) {
                    $prochain .= '<div class="prochainement_link">';
                    $prochain .= '<a href="';
                    $prochain .= $link;
                    $prochain .= '" title="informations supplémentaires"';
                    $prochain .= ' class="prochainement_link_itself">';
                    $prochain .= "(infos...)</a></div>";
                }

                $first = false;
            }

            $avenir .= "<tr><td>";

            $attachement = $ev['ATTACH'];
            if ($attachement) {
                $avenir .= '<div class="image_section">';
                $avenir .= '<a href="'.$attachement;
                $avenir .= '" title="" class="avenir_image">';
                $avenir .= '<div class="avenir_image"><span class="helper"></span>';
                $avenir .= '<img src="'.$attachement.'"></div>';
                $avenir .= '</a></div>';
            }

            $avenir .= format_($ev['SUMMARY'], 'avenir_summary');

            $avenir .= format_(
                "le ".$fmt_avenir->format($ev['DTSTART']),
                'avenir_date'
            );

            $avenir .= format_($ev['LOCATION'], 'avenir_location');

            $avenir .= format_($ev['DESCRIPTION'], 'avenir_description');

            $link = $ev['URL'];
            if ($link) {
                $avenir .= '<div class="avenir_link">';
                $avenir .= '<a href="'.$link;
                $avenir .= '" title="informations supplémentaires" class="avenir_link_itself">';
                $avenir .= "(plus d'informations...)</a></div>";
            }

            $avenir .= "</td></tr>";
        }
        $avenir .= '</tbody></table>';

        file_put_contents('themes/scene/tpl/__calendar.html', $avenir);
        file_put_contents('themes/scene/tpl/__prochainement.html', $prochain);
        return;
    }

    public static function get_or_create_schedule() {
        // assess cache situation of calendar,
        // then either use it or recreate data
        $ICS = 'public/calendar.ics';
        if (file_exists($ICS)) {
            if (time() - filemtime($ICS) < 300) {
                // use cached schedule
                return;
            }
        }

        tplSceneTheme::create_calendar_fragment();
    }
}
